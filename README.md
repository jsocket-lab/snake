# Snake
Play the classic game of snake with up to 4 players, using a device as a joystick to move your snake around to grab the blocks. 

### Installation
Node.js must be installed as a pre-requisite to run the project. You can download it from [here](https://nodejs.org/en/download/).


### Usage
1. Run this command to run the project.
```
node index.js
```

2. If everything is working correctly, you should see this:
```
Listening on port 8114!
```

3. If not, try `npm install` and run `node index.js` again.

4. Once it is working, type `http://IP_ADDRESS:8114/screen` into the address bar of any device that is on the same network as the device with the terminal. You should see three lines saying `WAITING PLAYERS`, `Connected Players: 0`, and `Players ready for game: 0`.

##### Connecting Other Players
1. To connect another player via another screen (can be any operating system), first make sure the device is on the same network as the screen device. 

2. Type `http://IP_ADDRESS:8114/controller` into the address bar. 

3. You should see `Galaxy Snake` at the top, a background color for the heading (that is your snake color), 3 buttons to `Pause`, `Restart`, and be `Ready`, and 4 buttons for directions. 


### Playing the Game
Use the provided arrows to change the direction that the snake is moving.  

##### `Pause`, `Restart`, and `Ready` buttons
1. The game will only start once all players are ready

2. Reselecting “Ready” will make you not ready, possibly stopping the game if it is selected in the middle of a round

3. Pause pauses the game

4. Restart restarts the game, requiring all players to select “ready” again before playing

##### Making sure your snake doesn’t die
Your snake can die by 
1. Crashing into its own tail
2. Crashing into other snakes tails

*This can mean that you can eliminate other snakes by growing longer and getting in their way!* 


### Other Features
##### Extending the Gaming Area
1. Type `http://IP_ADDRESS:8114/screen` into however many browser tabs wanted to extend the gaming area. Make sure that these devices are on the same network. 



